package dribblefollowers

import org.openqa.selenium.By
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.PageFactory
import spock.lang.Specification
import sun.awt.windows.WEmbeddedFrame

class DribbbleMainPage {
    WebDriver driver
    @FindBy(id = 't-shots')
    private WebElement shots
    @FindBy(css = 'li.more.active')
    List<WebElement> shotsList

    void goShots(String shotsName) {

        Actions actions = new Actions(driver)
        actions.moveToElement(shots).perform()
        //shots.click()
        shots.findElement(By.xpath('./ul/li[1]/a')).click()
        actions.moveToElement(shotsList.find { it.text.contains('Shots') }).perform()
        actions.click(shotsList.find { it.text.contains('Shots') }.findElements(By.xpath('ul/li/a')).find {
            it.getAttribute('text').contains(shotsName)
        }).perform()


    }
    @FindBy(id = 'main')

    private WebElement dribbbleShots

    @FindBy(css = 'a.url')
    private WebElement urlUser
//    @FindBy(css = 'span.team-avatar')
//    private WebElement url
    @FindBy(css = 'span.shot-byline-user')
    WebElement user
    @FindBy(css = 'div.slat-header')
    WebElement url
    @FindBy(className = 'slat-header user team-shot')
    WebElement head

    void commentShotsAndFollowing(String comment, String shotsName) {
        Utils.waitForLoad(driver)
        
        int length = dribbbleShots.findElements(By.xpath('./ol/li')).size()
        Random random = new Random()
        for (int i = 28; i < length; i++) {

            Utils.waitForLoad(driver)
            Actions actions = new Actions(driver)
            actions.moveToElement(dribbbleShots.findElements(By.xpath('./ol/li'))[i]).perform()
            dribbbleShots.findElements(By.xpath('./ol/li'))[i].click()
            Utils.waitForLoad(driver)
            DribbbleUserPage dribbbleUserPage = PageFactory.initElements(driver, DribbbleUserPage)
            try {
                user.findElement(By.xpath('./a')).click()
            }catch (NoSuchElementException e){
                dribbbleUserPage.commentText.sendKeys(comment)
                dribbbleUserPage.postComment.click()
            }


//            Actions actions = new Actions(driver)
//            actions.moveToElement(urlUser).perform()
            //Thread.sleep(1000)
            //  url.findElement(By.xpath('./a/picture/img')).click()
            // url.findElement(By.xpath('../h2/span/span/a[@class="url hoverable"]')).click()
            dribbbleUserPage.driver = driver
            dribbbleUserPage.clickFollow(comment, shotsName)
            Thread.sleep(random.nextInt(7000))

            //urlUser.findElement(By.xpath('./h2/span[0]/span/a[@rel="contact"]')).click()

//            driver.navigate().back()
//            driver.navigate().back()
        }
    }

}
