package dribblefollowers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.FindBy

import javax.el.MethodNotFoundException
import javax.swing.Action

class DribbbleUserPage {
    WebDriver driver
    @FindBy(css = 'a.form-btn')
    List<WebElement> buttons
    @FindBy(id = 'comment_text')
    WebElement commentText
    @FindBy(id='post-comment-btn')
    WebElement postComment
    void clickFollow(String comment, String shotsName){
        try{

            buttons.find {it.text =='Follow'}.click()

            driver.navigate().back()
            Utils.waitForLoad(driver)
            Actions actions = new Actions(driver)
            actions.moveToElement(commentText).perform()
            Utils.waitForLoad(driver)

            commentText.sendKeys(comment)
            postComment.click()
            Utils.waitForLoad(driver)
            driver.navigate().back()
            while (driver.getCurrentUrl().contains(shotsName.toLowerCase()) == false){
                driver.navigate().back()
//            }else {
//                driver.navigate().back()
            }

//
        }catch (NullPointerException e){
            driver.navigate().back()
            driver.navigate().back()
        }


    }
}
