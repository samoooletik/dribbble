package dribblefollowers

import org.openqa.selenium.WebDriver
import org.openqa.selenium.support.PageFactory
import spock.lang.Specification

class Main extends Specification {
    WebDriver driver = Utils.simpleDriver
//    static void main(){
//
//    }
    def 'Добавить фоловеров Вове'() {

        given: 'Открыта главная страница дрибл из под Вовы'
        Utils.openApartmentsPage(driver)
        when: 'Shots -> Popular '
        Utils.waitForLoad(driver)
        DribbbleMainPage dribbbleMainPage = PageFactory.initElements(driver, DribbbleMainPage)
        Utils.waitForLoad(driver)
        dribbbleMainPage.driver=driver
        ['Debuts','Team Shots', 'Playoffs', 'Rebounds', 'Animated GIFs','Shots with Attachments'].each {
            dribbbleMainPage.goShots("${it}")
            dribbbleMainPage.commentShotsAndFollowing('awesome!', "${it}")
        }



        then: ''
    }
}
