package dribblefollowers

import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.support.ui.WebDriverWait

import java.util.concurrent.TimeUnit

class Utils {
    static WebDriver getSimpleDriver() {
        ChromeOptions options = new ChromeOptions()
        options.addArguments("start-maximized")
        options.addArguments("disable-infobars")
        options.addArguments("--disable-extensions")
        //    options.addArguments("--headless")

        return new ChromeDriver(options)
    }

    static openApartmentsPage(WebDriver driver) {
//        driver.get('http://deploy:;kl$1asd234asdf@test.spacemir.com')
//        Thread.sleep(222)
        driver.get('https://dribbble.com/')

        def signIn = driver.findElement(By.id('t-signin'))
        signIn.click()

        driver.get('https://dribbble.com/session/new')

        waitForLoad(driver)
        def emailInput = driver.findElement(By.id('login'))
        emailInput.sendKeys('vladimir-palyanov')
        def passwordInput = driver.findElement(By.id('password'))
        passwordInput.sendKeys('webvooova')
        def login = driver.findElement(By.cssSelector('input.button[value="Sign In"]'))
        login.click()

//        for( int i=0; i<login.size(); i++){
//            Boolean y = login[i].getText()=='Sign In'
//            //println(y)
//            if (y==true){
//                login[i].click()
//                break
//            }
//        }
        //Ждем полной загрузки страницы и еще 3 секунды для появления кнопки
        waitForLoad(driver)
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS)
        Thread.sleep(1000)
        //Utils.getWaitDriver(driver).until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector('body > app-component > sm-private-route > div > div.app-content > div > sm-apartments > div > div > section > div.d-flex.m-t-md > div.w-50.setApp__centerBlockAside--lilac.r-b.no-r-r.setApp__centerBlockAside--inFooter.p-l-lg.p-r-lg.p-t-lg > footer > button.btn.btn--sq.btn--app.btn--appWhite')))
        // By.cssSelector('body > app-component > private-route > div > div.app-content > div > apartments > div > div > section > div.d-flex.m-y-lg.top-block > div.w-50.p-l-lg > button')
    }

    static void waitForLoad(WebDriver driver) {
        new WebDriverWait(driver, 30)
                .until({ wd ->
            ((JavascriptExecutor) wd).executeScript("return document.readyState").equals("complete")
        })
        Thread.sleep(1000)
    }

}
